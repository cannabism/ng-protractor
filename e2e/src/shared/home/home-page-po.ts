import {browser, by, element} from 'protractor';

export class HomePage {
  static navigateTo() {
    return browser.get('/');
  }

  static getNumberHeroes() {
    return element.all(by.css('#heroes-list mat-card')).count();
  }

  static getHeader() {
    return element(by.css('h1.header__title')).getText();
  }

  static getHeroCards() {
    return element.all(by.css('#heroes-list mat-card'));
  }

  static getVoteNumber(cardNumber: number) {
    return this.getHeroCards().get(cardNumber).element(by.css('.hero-actions')).getText().then(text => {
      return text.split('\n')[0];
    });
  }

  static clickVote(cardNumber: number) {
    return this.getHeroCards().get(cardNumber).element(by.css('mat-icon')).click();
  }
}
