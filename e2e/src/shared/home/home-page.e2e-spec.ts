import {HomePage} from './home-page-po';
import {AppConfig} from '../../../../src/app/configs/app.config';
import {browser} from 'protractor';


describe('Home page', function () {
  let page;

  beforeEach(() => {
    page = new HomePage();
  });

  it('should contains heroes limit', () => {
    HomePage.navigateTo();
    expect<any>(HomePage.getNumberHeroes()).toBe(AppConfig.topHeroesLimit);
  });

  it('should have Heroes most liked title', () => {
    HomePage.navigateTo();
    expect(HomePage.getHeader()).toBe('Heroes most liked');
  });

  it('should increase number after clicking on vote', async () => {
    HomePage.navigateTo();
    const voteNumber = Number(await HomePage.getVoteNumber(0));
    HomePage.clickVote(0);
    browser.sleep(1000);
    const newVoteNumber = Number(await HomePage.getVoteNumber(0));
    expect(newVoteNumber).toBe(voteNumber + 1);
  });

});
